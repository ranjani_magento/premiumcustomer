<?php

class Ranjani_Premiumcustomer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_PREMIUMCUSTOMER_GENERAL_ISENABLED = 'premium_delivery/premium_delivery_general/premium_delivery_enable';
    const XML_PATH_PREMIUMCUSTOMER_GENERAL_PRODUCTID = 'premium_delivery/premium_delivery_general/premium_delivery_product';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_PREMIUMCUSTOMER_GENERAL_ISENABLED);
    }

    public function getProductId()
    {
        return Mage::getStoreConfig(self::XML_PATH_PREMIUMCUSTOMER_GENERAL_PRODUCTID);
    }

    public function getProductIds()
    {
        $ids_raw = Mage::getStoreConfig(self::XML_PATH_PREMIUMCUSTOMER_GENERAL_PRODUCTID);
        if ($ids_raw && $ids_raw != '') {
            return explode(',', $ids_raw);
        } else {
            return array();
        }
    }

    function productInCart($productId)
    {
        $cart = Mage::helper('checkout/cart')->getCart();
        foreach ($cart->getItems() as $item) {
            if ($item->getProduct()->getId() == $productId) {
                return true;
            }
        }
        return false;
    }

    function isPremiumDeliveryProductInCart()
    {
        if ($this->isEnabled() && $this->productInCart($this->getProductId())) {
            return true;
        }
        return false;
    }

    function isValidExtraProduct($productId)
    {
        $ids = $this->getProductIds();
        if (in_array($productId, $ids)) {
            return true;
        }
        return false;
    }

    function hasProducts()
    {
        if (count($this->getProductIds()) > 0) {
            return true;
        }
        return false;
    }

    function getDeliveryProducts()
    {
        $items = array();
        foreach ($this->getProductIds() as $id) {
            if ($id != '') {
                try {
                    $item = Mage::getModel('catalog/product')->load($id);
                } catch (Exception $e) {
                    continue;
                }
                if ($item->getId()) {
                    $items[] = $item;
                }
            }
        }
        return $items;
    }
}