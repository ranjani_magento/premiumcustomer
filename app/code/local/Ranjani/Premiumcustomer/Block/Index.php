<?php

class Ranjani_Premiumcustomer_Block_Index extends Mage_Core_Block_Template
{
    public function getHelper()
    {
        $premiumCustomerHelper = Mage::helper('premiumcustomer');
        return $premiumCustomerHelper;
    }

    public function getCustomerSession()
    {
        $customerSession = Mage::getSingleton("customer/session");
        return $customerSession;
    }
}