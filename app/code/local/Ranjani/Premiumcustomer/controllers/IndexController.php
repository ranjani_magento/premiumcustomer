<?php

class Ranjani_Premiumcustomer_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("premium-delivery"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));
        $breadcrumbs->addCrumb("premium-delivery", array(
            "label" => $this->__("premium-delivery"),
            "title" => $this->__("premium-delivery")
        ));
        $this->renderLayout();
    }
}