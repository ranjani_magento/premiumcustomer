<?php

class Ranjani_Premiumcustomer_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function addVirtualProductAction()
    {
        $helper = Mage::helper('premiumcustomer');
        $product_id = $this->getRequest()->getPost('product_id');
        $remove = $this->getRequest()->getPost('remove', false);
        $customerSession = Mage::getSingleton("customer/session");
        if ($customerSession->isLoggedIn()) {
            if ($helper->isValidExtraProduct($product_id)) {
                if (!$remove) {
                    try {
                        /* Add product to cart if it doesn't exist */
                        $product = Mage::getModel('catalog/product')->load($product_id);
                        $cart = Mage::getSingleton('checkout/cart');
                        $cart->addProduct($product);
                        $cart->save();
                        echo "added";
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                } else {
                    $items = Mage::helper('checkout/cart')->getCart()->getItems();
                    foreach ($items as $item) {
                        if ($item->getProduct()->getId() == $product_id) {
                            Mage::helper('checkout/cart')->getCart()->removeItem($item->getId())->save();
                        }
                    }
                }
            }
        } else {
            echo "notloggedin";
        }
    }
}
