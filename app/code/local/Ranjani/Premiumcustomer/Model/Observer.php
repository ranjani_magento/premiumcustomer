<?php

class Ranjani_Premiumcustomer_Model_Observer
{
    public function saveOrderAfter(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('checkout/session')->getPremiumCustomerDeliveryFlag()) {
            $order = $observer->getOrder();
            $helper = Mage::helper('premiumcustomer');
            if (($order->getShippingMethod() === 'pltshipping_pltshipping' && $order->getBaseShippingAmount() > 0) ||
                ($helper->isPremiumDeliveryProductInCart() && $helper->isEnabled())
            ) {
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
                $expiryDate = $customer->getFreeShippingExpires();
                $renewalCount = $customer->getRenewalCount();
                if (is_null($expiryDate) || empty($expiryDate)) {
                    $expiryDate = date('Y-m-d 00:00:00', strtotime("+12 months"));
                    $renewalCount = 0;
                } else {
                    $expiryDate = date($expiryDate, strtotime("+12 months"));
                }
                $renewalCount = $renewalCount + 1;
                $startDate = $customer->getFreeShippingStart();
                if (is_null($startDate) || empty($startDate)) {
                    $startDate = date('Y-m-d 00:00:00');
                }
                $customer->setFreeShippingExpires($expiryDate);
                $customer->setRenewalCount($renewalCount);
                $customer->setFreeShippingStart($startDate);
                $customer->setPremiumCustomerActive('1');
                $customer->save();
            }
            Mage::getSingleton('checkout/session')->setPremiumCustomerDeliveryFlag(false);
        }
    }
}
