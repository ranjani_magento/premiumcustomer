<?php
$installer = $this;
$installer->startSetup();
$websites = Mage::app()->getWebsites();
$siteIds = Array();
$storeIds = Array();
foreach ($websites as $site) {
    $siteIds[] = $site->getId();
    foreach ($site->getGroups() as $group) {
        $stores = $group->getStores();
        foreach ($stores as $store) {
            $storeIds[] = $store->getId();
        }
    }
}
$rand = rand(1, 9999);
$product = Mage::getModel('catalog/product');
$product->setStoreId(0); //use this instead of Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$product->setWebsiteIds($siteIds);
$product->setTypeId('virtual');
$product->addData(array(
    'name' => 'Premium customer Unlimited Next Day delivery Purchase',
    'attribute_set_id' => $product->getDefaultAttributeSetId(),
    'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
    'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
    'meta_title' => 'Premium customer Unlimited Next Day delivery Purchase',
    'weight' => '1',
    'sku' => 'YEARLY-SUBSCRIPTION' . $rand,
    'price' => '5.99',
    'tax_class_id' => 2,
    'description' => 'Premium customer Unlimited Next Day delivery Purchase',
    'short_description' => 'Unlimited Next Day Delivery - (1 Year)',
    'stock_data' => array(
        'manage_stock' => 1,
        'qty' => '100000000000', //set the qty
        'use_config_manage_stock' => 1,
        'use_config_min_sale_qty' => 1,
        'use_config_max_sale_qty' => 1,
        'use_config_enable_qty_increments' => 1,
        'is_in_stock' => '1'
    ),
));
$product->save();
Mage::getConfig()->saveConfig('premium_delivery/premium_delivery_general/premium_delivery_product', $product->getId());
$installer->endSetup();
	 