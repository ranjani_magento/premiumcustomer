<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute("customer", "free_shipping_start", array(
    "type" => "datetime",
    "backend" => "",
    "label" => "Free Shipping Starts",
    "input" => "text",
    "source" => "",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    "note" => "Date that this customers free next day shipping started"
));
$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "free_shipping_start");
$used_in_forms = array();
$used_in_forms[] = "adminhtml_customer";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100);
$attribute->save();
$installer->addAttribute("customer", "premium_customer_active", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Premium shipping active",
    "input" => "text",
    "source" => "",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    "note" => "active flag 1 for active and 0 for inactive"
));
$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "premium_customer_active");
$used_in_forms = array();
$used_in_forms[] = "adminhtml_customer";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100);
$attribute->save();
$installer->addAttribute("customer", "renewal_count", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Renewal Count",
    "input" => "text",
    "source" => "",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    "note" => "Number of times premium shipping was renewed"
));
$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "renewal_count");
$used_in_forms = array();
$used_in_forms[] = "adminhtml_customer";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100);
$attribute->save();
$installer->endSetup();
	 