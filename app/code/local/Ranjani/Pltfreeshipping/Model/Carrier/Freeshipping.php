<?php

class Ranjani_Pltfreeshipping_Model_Carrier_Freeshipping extends Mage_Shipping_Model_Carrier_Flatrate
{
    protected $_code = 'pltshipping';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $premiumCustomerHelper = Mage::helper('premiumcustomer');
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $pltFreeShipping = false;
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if (!stristr($customer->getEmail(), "@Ranjani.com") && $this->getConfigData('testmode') == '1') {
                return false;
            }
            $expiryDate = $customer->getFreeShippingExpires();
            if (($this->getConfigData('signupdisabled') == '1') && ($expiryDate == null || strtotime($expiryDate) <= strtotime(now()))) {
                return false;
            }
            if ($expiryDate !== null && (strtotime($expiryDate) >= strtotime(now()))) {
                $pltFreeShipping = true;
            }
            if ($premiumCustomerHelper->isPremiumDeliveryProductInCart()) {
                $pltFreeShipping = true;
            }
        } else {
            return false;
        }
        $result = Mage::getModel('shipping/rate_result');
        $method = Mage::getModel('shipping/rate_result_method');
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('title'));
        $shippingPrice = $this->getConfigData('price');
        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);
        if ($pltFreeShipping === true) {
            $method->setMethodTitle($this->getConfigData('title2'));
            $method->setPrice(0.00);
        }
        $result->append($method);
        return $result;
    }

    public function getAllowedMethods()
    {
        return array($this->_code => $this->getConfigData('name'));
    }
}

?>
